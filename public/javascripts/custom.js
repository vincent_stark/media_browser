VK.init({apiId: 2728031});

function getAge(dateString) {
    var today = new Date();
    var dateReg = /(\d).(\d).(\d{4})/g;
    var dateArray = dateReg.exec(dateString);
    var birthDate = new Date(
      dateArray[3],
      dateArray[2]-1, // Careful, month starts at 0!
      dateArray[1]
    );
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function limitText(text, maxLength) {
  
  if (text.length > maxLength) {
    text = text.substr(0, maxLength) + '...';
  }

  return text;

}
