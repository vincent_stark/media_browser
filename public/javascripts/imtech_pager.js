var Imtech = {};
Imtech.Pager = function() {
    this.paragraphsPerPage = 3;
    this.currentPage = 1;
    this.pagingControlsContainer = '#pagingControls';
    this.pagingContainerPath = '#content';

    this.numPages = function() {
        var numPages = 0;
        if (this.paragraphs != null && this.paragraphsPerPage != null) {
            numPages = Math.ceil(this.paragraphs.length / this.paragraphsPerPage);
        }

        return numPages;
    };

    this.showPage = function(page) {
        this.currentPage = page;
        var html = '';

        this.paragraphs.slice((page-1) * this.paragraphsPerPage,
            ((page-1)*this.paragraphsPerPage) + this.paragraphsPerPage).each(function() {
            html += '<div class="media_entry">' + $(this).html() + '</div>';
        });

        $(this.pagingContainerPath).html(html);

        renderControls(this.pagingControlsContainer, this.currentPage, this.numPages());
    }

    this.prevPage = function() {
      if (this.currentPage > 1) {
        this.showPage(this.currentPage - 1);
      }
    }

    this.nextPage = function() {
      if (this.currentPage < this.numPages()) {
        this.showPage(this.currentPage + 1);
      }
    }

    var renderControls = function(container, currentPage, numPages) {
        var pagingControls = 'Page: <ul>';

        if (currentPage > 1) {
          pagingControls += '<li><a href="#" onclick="pager.prevPage(); return false;">&lt;&nbsp;Prev</a></li>';
        } else {
          pagingControls += '<li>&lt;&nbsp;Prev</li>';
        }

        if (numPages < 10) {
	        for (var i = 1; i <= numPages; i++) {
	            if (i != currentPage) {
	                pagingControls += '<li><a href="#" onclick="pager.showPage(' + i + '); return false;">' + i + '</a></li>';
	            } else {
	                pagingControls += '<li>' + i + '</li>';
	            }
	        }
        } else {
          if (currentPage != 1) {
	        pagingControls += '<li><a href="#" onclick="pager.showPage(1); return false;">1</a></li>';
          } else {
            pagingControls += '<li>1</li>';
          }
          if (currentPage <= 4) {
            for (var i = 2; i <= 4; i++) {
	            if (i != currentPage) {
	                pagingControls += '<li><a href="#" onclick="pager.showPage(' + i + '); return false;">' + i + '</a></li>';
	            } else {
	                pagingControls += '<li>' + i + '</li>';
	            }
	        }
            pagingControls += '<li>...</li>';
          } else if(currentPage < numPages-4) {
            pagingControls += '<li>...</li>';
            pagingControls += '<li><a href="#" onclick="pager.showPage(' + (currentPage - 1) + '); return false;">' + (currentPage - 1) + '</a></li>';
            pagingControls += '<li>' + currentPage + '</li>';
            pagingControls += '<li><a href="#" onclick="pager.showPage(' + (currentPage + 1) + '); return false;">' + (currentPage + 1) + '</a></li>';
            pagingControls += '<li><a href="#" onclick="pager.showPage(' + (currentPage + 2) + '); return false;">' + (currentPage + 2) + '</a></li>';
            pagingControls += '<li>...</li>';
          } else {
            pagingControls += '<li>...</li>';
            for (var i = numPages-4; i <= numPages-1; i++) {
	            if (i != currentPage) {
	                pagingControls += '<li><a href="#" onclick="pager.showPage(' + i + '); return false;">' + i + '</a></li>';
	            } else {
	                pagingControls += '<li>' + i + '</li>';
	            }
            }
          }
          if (currentPage != numPages) {
            pagingControls += '<li><a href="#" onclick="pager.showPage(' + numPages + '); return false;">' + numPages + '</a></li>';
          } else {
            pagingControls += '<li>' + numPages + '</li>';
          }
        }

        if (currentPage < numPages) {
          pagingControls += '<li><a href="#" onclick="pager.nextPage(); return false;">Next&nbsp;&gt;</a></li>';
        } else {
          pagingControls += '<li>Next&nbsp;&gt;</li>';
        }

        pagingControls += '</ul>';

        $(container).html(pagingControls);
    }
}
